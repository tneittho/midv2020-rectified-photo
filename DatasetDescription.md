# "*Rectified photos*" dataset description

This dataset is part of the well-know [MIDV2020](http://l3i-share.univ-lr.fr/MIDV2020/midv2020.html) database. The dataset *"templates"* of MIDV2020 contains the images of the different documents in full pages with the annotations of the position of the fields as well as their corresponding text label and field type. However, this dataset does not reflect real capture conditions such as lightning conditions or deformations. Furthermore, MIDV2020 contains a *"photos"* dataset made of identity documents (IDs) in real capture conditions but that does not contain annotations at the field level. To that end, we propose this *"rectified photos"* dataset to perform text localisation and recognition on IDs.

## Construction of the dataset

To create this dataset, each document image, cropped from the *"photos"* dataset, is rectified to the dimensions of the same document image from the *"templates"* dataset. The annotations of the text field positions coming from the *"templates"* dataset. The annotations of the text field positions coming from the *"template"* are then manually reannotated to match the localisation of the text fields from the resized document.

We also design the field structure (template /!\ different from the *"templates"* dataset) of each ID type thanks to the text field positions from the *"templates"* dataset by extending the field bounding boxes to their theoretical maximum size.

## Composition of the dataset

The dataset is composed of the same $1 000$ documents as the others MIDV2020 datasets, split into $10$ document types.
It presents the same distribution per document type as the *"photos"* dataset in terms of capture variations:
- 20 documents in low lightning conditions,
- 20 documents with high projective distortions (the documents are rectified, but this still introduces potential interpolation errors),
- 10 documents with highlights,
- 10 documents in natural lightning conditions,
- 40 documents with various background (the documents are rectified so the background is not present).

As some characters in the dataset occur a very small number of times (sometimes only once), we design a mapping when possible between the characters ("à" becomes "a" for example). We do the same for characters that are graphicaly the same (latin "P" and russian "Р" for example). When this is not possible and that the number of instances of this character is very low, we do not consider this field for the recognition for training and testing. The different characters as well as their mapping and whether they are  considered or not, are present in the file *alphabet.json*.

```json
{
    "alphabet": ["a","b",...],
    "mapping": {
        "\u00c7": "C",
        ...
    },
    "deleted": ["$",";",...],
    "count": {"a": 42869, "b":5969, ...}
}
```

- alphabet correspond to the characters that are to be recognized by the system,
- mapping correspond to the equivalent character in the alphabet ("\u00c7" will be interpreted as "C"),
- deleted correspond to the characters to not take in account,
- count correspond to the total number of the character occurence in the dataset

The file *annotations.json* present when loading the datas contains the annotations for each document:

```json
{
    "img_name": {
        "doc_type": <doc_type>,
        "camera_type": <camera_type>,
        "capture_condition": <0-8>,
        "fields": [
            {
                "x1": 717,
                "y1": 977,
                "x2": 799,
                "y2": 1020,
                "label": "MB",
                "map_label": "MB",
                "type": "authority"
            },
            ...
        ]
    },
    ...
}
```

- doc_type correspond to the type of the document,
- camera_type is the type of the camera that perormed the capture,
- capture_condition correspond to the capture conditions presented in MIDV2020 paper from 0 to 8,
- x1,x2,y1,y2 correspond to the coordinates of the box,
- label correspond to the textual ground truth as presented in the image;
- map_label correspond to the mapping from the *alphabet.json* file;
- type correspon to the type of the field.

The file *templates.json* contains the description of the templates for each kind of document:

```json
{
    "doc_type": {
        "h": 1360,
        "w": 2167,
        "fields": [
            {
                "x1": 708,
                "y1": 971,
                "x2": 1416,
                "y2": 1019,
                "type": "authority"
            },
            ...
        ]
    },
    ...
}
```

- h correspond to the height of the template,
- w correspond to the width of the template.

The images are under a single folder and are named under the format: "<doc_type>_<doc_number>".
