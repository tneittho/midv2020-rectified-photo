from metrics.IoU import calc_iou
import torch

class DetectionMetrics:
    
    def __call__(self, curr_bb, curr_annots, overlap_threshold):
        IoU = calc_iou(curr_bb, curr_annots[:,:4])
        max_iou, _ = torch.max(IoU, dim=1)
        tot_iou = max_iou.sum()
        over_b = len(max_iou)

        nb_gt_b = curr_annots[:,:4].shape[0]
        nb_det_b = curr_bb.shape[0]
        nb_corr_det_b = (max_iou>overlap_threshold).sum()

        iou_thresh = [0.05*i for i in range(20)]
        rp_c = []
        for thresh in iou_thresh:
            nb_corr_found = (max_iou>thresh).sum()
            rp_c.append([thresh, nb_gt_b, nb_det_b, nb_corr_found])
        
        return tot_iou, over_b, nb_gt_b, nb_det_b, nb_corr_det_b, rp_c