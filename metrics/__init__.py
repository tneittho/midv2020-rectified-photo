from metrics.Associate_gt_pred import AssoGTTxtPenalty
from metrics.CER import CER_IoU, calc_cer
from metrics.IoU import calc_iou
from metrics.Precision import calc_bb_precision
from metrics.Recall import calc_bb_recall
from metrics.DetMetrics import DetectionMetrics