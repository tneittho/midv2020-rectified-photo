import torch
import numpy as np

def levenshtein(str1, str2, raw=False):
    if len(str1) == 0:
        return len(str2)
    if len(str2) == 0:
        return len(str1)
    distances = torch.zeros((len(str1)+1, len(str2)+1))

    for t1 in range(len(str1)+1):
        distances[t1][0] = t1
    for t2 in range(len(str2)+1):
        distances[0][t2] = t2
    
    a, b, c = 0, 0, 0
    
    for t1 in range(1, len(str1)+1):
        for t2 in range(1, len(str2)+1):
            if str1[t1-1] == str2[t2-1]:
                distances[t1][t2] = distances[t1-1][t2-1]
            else:
                a = distances[t1][t2-1]
                b = distances[t1-1][t2]
                c = distances[t1-1][t2-1]
                distances[t1][t2] = min([a,b,c]) + 1
    if raw:
        return distances
    return distances[len(str1), len(str2)].item()

def calc_cer(word_list, gt_list):
    """
    Compute the distance between two lists of words
    return the sum of the edit distances, the total length of the gt
    """
    distances = []
    for i in range(len(word_list)):
        dis = levenshtein(word_list[i], gt_list[i])
        distances.append(dis)
    
    return sum(distances), sum([len(i) for i in gt_list])

def CER_IoU(txt, txt_annots, bb, bb_annots, asso, thresh):
    """
    Compute the CER given the localisation & recognition and the association fonction
    """
    gt_dcd = asso(txt, txt_annots, bb, bb_annots, thresh)
    gt_w, words = zip(*gt_dcd)
    distances, gt_len = calc_cer(words, gt_w)
    return distances, gt_len

def CER_thresh(txt, txt_annots, bb, bb_annots, asso):
    """
    Compute the CER for different threshold of association
    """
    thresh = [0.05*i for i in range(20)]
    cer_iou = []
    for t in thresh:
        dist, gt_len = CER_IoU(txt, txt_annots, bb, bb_annots,asso, t)
        cer_iou.append([t,dist,gt_len])
    return cer_iou