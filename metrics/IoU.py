import torch

def calc_iou(a, b):
    area = (b[:, 2] - b[:, 0]) * (b[:, 3] - b[:, 1])
    try:
        iw = torch.min(torch.unsqueeze(a[:, 2], dim=1), b[:, 2]) - torch.max(torch.unsqueeze(a[:, 0], dim=1), b[:, 0])
    except TypeError as e:
        GlobalLogger.log(f"a: {a}\nb {b}", get_set()+"_"+get_name())
        raise e
    ih = torch.min(torch.unsqueeze(a[:, 3], dim=1), b[:, 3]) - torch.max(torch.unsqueeze(a[:, 1], dim=1), b[:, 1])

    iw = torch.clamp(iw, min=0)
    ih = torch.clamp(ih, min=0)

    ua = torch.unsqueeze((a[:, 2] - a[:, 0]) * (a[:, 3] - a[:, 1]), dim=1) + area - iw * ih

    ua = torch.clamp(ua, min=1e-8)

    intersection = iw * ih

    IoU = intersection / ua

    return IoU

def calc_diou(a, b):
    """
    Compute the Distance IoU
    """
    # a has shape: [nb_bb,4] / x1,y1,x2,y2
    # b has shape [nb_bbgt, 4]
    # DIoU = IoU + R(B,B*)
    # R(B,B*) = ro2(b,b*)/c
    # where b,b* are the coordinates of the center points of bb,
    # ro2 is the euclidean distance
    # and c is the diagonal length of the smallest enclosing box covering the two bb
    # sqrt( (max_x(b,b*) - min_x(b,b*))^2 + (max_y(b,b*) - min_y(b,b*))^2 )
    iou = calc_iou(a,b)

    # compute the center of each BB
    a_center_x = a[:,0] + (a[:,2]-a[:,0])/2
    a_center_y = a[:,1] + (a[:,3]-a[:,1])/2
    b_center_x = b[:,0] + (b[:,2]-b[:,0])/2
    b_center_y = b[:,1] + (b[:,3]-b[:,1])/2

    # compute the euclidean distance between the centers
    ro_bb = torch.sqrt( torch.pow(a_center_x.unsqueeze(1) - b_center_x,2) + torch.pow(a_center_y.unsqueeze(1) - b_center_y, 2) )

    # compute the coordinates of x1,y1,x2,y2 of the enclosing bb
    enclosing_x_max = torch.max(torch.unsqueeze(a[:,2], dim=1), b[:,2])
    enclosing_x_min = torch.min(torch.unsqueeze(a[:,0], dim=1), b[:,0])
    enclosing_y_max = torch.max(torch.unsqueeze(a[:,3], dim=1), b[:,3])
    enclosing_y_min = torch.min(torch.unsqueeze(a[:,1], dim=1), b[:,1])

    # compute the diagonal of the enclosing bb i.e. distance between the two previous points 1 & 2
    # as the computation of R need c^2, we remove the sqrt operation that would usually be used for computing the distance
    c = torch.pow(enclosing_x_max - enclosing_x_min, 2) + torch.pow(enclosing_y_max - enclosing_y_min, 2)
    R = ro_bb/c
    # diou = iou + R, iou

    return R, iou