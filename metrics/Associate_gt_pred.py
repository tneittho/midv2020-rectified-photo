from metrics.IoU import calc_iou
import torch

class AssoGTTxtPenalty:

    def __call__(self, txt, txt_annots, bb, bb_annots, thresh):
        
        IoU = calc_iou(bb_annots, bb)
        max_IoU, argmax_IoU = torch.max(IoU, dim=1)

        corr_detect = max_IoU > thresh # shape (nb detected)
        # association has shape [nb asso, (gt text, det text, max iou, argmax iou)]
        bbgt_bbdt = []
        bbgt_solo = []
        bbdt_solo = []

        for i in range(len(corr_detect)):
            if corr_detect[i]: # the bbgt overlaps over 0.8 with a bbdet
                bbgt_bbdt.append([txt_annots[i], txt[argmax_IoU[i]], max_IoU[i], argmax_IoU[i]])
            else: # no bbdet overlap with the bbgt
                bbgt_solo.append([txt_annots[i], "", 0, -1])
        
        cleaned, duplicates = self._remove_duplicate(bbgt_bbdt)
        bbgt_bbdt = cleaned
        bbgt_solo.extend(duplicates)

        bbdt_indices = range(len(txt))
        associated_bbdt_indices = [a[-1] for a in bbgt_bbdt]
        bbdt_solo_indices = [bbdt_i for bbdt_i in bbdt_indices if bbdt_i not in associated_bbdt_indices]
        bbdt_solo = [["", txt[i], 0, i] for i in bbdt_solo_indices]
        associations = bbgt_bbdt + bbgt_solo + bbdt_solo

        return [(a[0], a[1]) for a in associations]

    def _remove_duplicate(self, bbgt_bbdt):
        bbgt_bbdt.sort(key=lambda x: x[2], reverse=True) #sorted by overlap in descending order
        cleaned = []
        duplicate = []
        for i in range(len(bbgt_bbdt)):
            # if the bbgt is associated to a bbdt already present in the cleaned, then the bbgt is associated to an empty prediction
            if bbgt_bbdt[i][-1] in [a[-1] for a in cleaned if a[-1] != -1]: 
                duplicate.append([bbgt_bbdt[i][0], "", 0, -1])
            else:
                cleaned.append(bbgt_bbdt[i])
        return cleaned, duplicate
    