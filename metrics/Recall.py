import torch
from metrics.IoU import calc_iou

def calc_bb_recall(bb_preds, bb_gt, threshold=0.7):
    """
    nb boxes correctly found over nb of boxes to find
    a GT box is correctly found if it's IoU with a pred box is over a threshold
    """
    IoU = calc_iou(bb_gt, bb_preds)
    # has shape [bb_gt, bb_preds]
    max_IoU, argmax_IoU = torch.max(IoU, dim=1)
    # max_IoU is the maximum overlap of a GT with the various predictions
    nb_boxes_corr_found = (max_IoU>threshold).sum()
    # compute if the maximum IoU of each GT is overlapping eanough and count then
    recall = nb_boxes_corr_found/bb_gt.shape[0]
    return recall
