import argparse
import os
import pytorch_lightning as pl
import random
import numpy as np
import torch

from pathlib import Path
from pytorch_lightning.callbacks import ModelCheckpoint

from data import RectPhotoDataloader, RectPhotoDataset
from architecture import SampleArchi

def set_loaders(args):
    train_dataset = RectPhotoDataset(
        args.database_path, mode="train", fold=args.fold
    )
    val_seen_dataset = RectPhotoDataset(
        args.database_path, mode="val_seen", fold=args.fold
    )
    val_unseen_dataset = RectPhotoDataset(
        args.database_path, mode="val_unseen", fold=args.fold
    )
    test_seen_dataset = RectPhotoDataset(
        args.database_path, mode="test_seen", fold=args.fold
    )
    test_unseen_dataset = RectPhotoDataset(
        args.database_path, mode="test_unseen", fold=args.fold
    )

    train_dataset.compose()
    val_seen_dataset.compose()
    val_unseen_dataset.compose()
    test_seen_dataset.compose()
    test_unseen_dataset.compose()

    dataloader = RectPhotoDataloader(train_dataset, [val_seen_dataset, val_unseen_dataset], [test_seen_dataset, test_unseen_dataset], 3, 500)

    return dataloader

def init_model(args, str_converter):
    model_parameters = {
        "str_converter": str_converter,
        "pretrain": True,
        "image_chan": 1,
        "max_boxes": 20,
        "score_threshold":0.90,
        "overlap_threshold": 0.1
    }
    model = SampleArchi(model_parameters)

    return model

def get_best_model(args, str_converter):
    model_parameters = {
        "str_converter": str_converter,
        "pretrain": True,
        "image_chan": 1,
        "max_boxes": 20,
        "score_threshold":0.90,
        "overlap_threshold": 0.1
    }
    path_to_load = f"model_save/{args.net_name}/best.ckpt"
    model_best = model.load_from_checkpoint(checkpoint_path=path_to_load, kwargs=model_parameters)
    return model_best

def callbacks(args):
    checkpoint_callbacks = ModelCheckpoint(
        dirpath=f"model_save/{args.net_name}",
        monitor="average_cer_mix_val",
        filename="best",
        save_last=True,
        save_top_k=1,
        mode="min",
        save_on_train_epoch_end=True
    )
    return [checkpoint_callbacks]

if __name__ == "__main__":
    parser = argparse.ArgumentParser(add_help=True)
    parser.add_argument("-d", "--database_path", type=str, required=True, help="Path to the database to use")
    parser.add_argument("-n", "--net_name", type=str, required=True, help= "Name of the network /!\ network architecture")
    parser.add_argument("-f", "--fold", type=int, choices=list(range(9)), required=True, help="the fold to use to train the system")
    parser.add_argument("-b", "--batch_size", type=int, default=2, help="Batch size to use")

    args = parser.parse_args()

    random.seed(1234)
    np.random.seed(1234)
    torch.manual_seed(1234)

    dataloader = set_loaders(args)
    model = init_model(args, dataloader.train_dataset.textConverter)

    logger = pl.loggers.TensorBoardLogger("./logs")

    trainer = pl.Trainer(
        accelerator="gpu",
        devices=1,
        max_epochs=200,
        precision=32,
        enable_progress_bar=True,
        logger=logger,
        gradient_clip_val=0.5,
        num_sanity_val_steps=0,
        callbacks=callbacks(args)
    )

    trainer.fit(model, dataloader)
    print("Training end")
    print("Test start")
    model = get_best_model(args, dataloader.train_dataset.textConverter)
    trainer.test(model, dataloader.test_dataloader())
    print("Test end")


