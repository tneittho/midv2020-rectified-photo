# MIDV2020 rectified photo

This repository contains a sample code for the new dataset *"rectified photos"* presented in the paper [Knowledge Integration inside Neural Networks for Unseen ID Types Analysis](https://link.springer.com/chapter/10.1007/978-3-031-41501-2_21).
This dataset is based on the [MIDV2020](https://arxiv.org/abs/2107.00396) database that can be found at: <http://l3i-share.univ-lr.fr/MIDV2020/midv2020.html> .

## Installing the project

To run this project, you can setup your venv/conda environment and run:

`install.sh`

It contains the installation commands (pip instructions).

## Running sample code

To first run and test the project, run the script `Training.py`

The parameters to give are:

- `--database_path` (`-d`): the path to the database you want to use
- `--net_name` (`-n`): the name to give to your network
- `--fold` (`-f`): which fold to run on (0-9 for "Rectified Photos" database)
- `--batch_size` (`-b`): the size of the batch

example:

`python Training.py -d ~/Documents/rectified_photos/ -n ExampleNet -f 2 -b 3`

## Contacts

<timothee.neitthoffer@idnow.io> (if I don't answer, try with @gmail)

## Project structure

- architecture
  - SampleArchi
    - Toy architecture with two networks: one for localisation, one for recognition
  - Backbone + Up
    - Components of the upper architectures
  - Segmentation
  - RecognitionHead

  - Loss
    - BCE
    - adapted CTC
      - associate the ground truth to the predictions via the overlapping of their bounding boxes

- data
  - Collate
    - How to obtain a consistant batch (resizing and stuff for images and gt)
  - RectPhotoDataset
    - torch dataset to load the images from the "Rectified Photos" dataset
  - RectPhotoDataLoader
    - torch_lightning data module to present the train, val, test dataloaders

- metrics
  - IoU / Precision / Recall
  - DetMetrics
    - compute the true positives, true negatives, false positives and IoUs for later computing Recall, Precision, mean IoU
  - CER
    - CER modified similarly to the CTC so the association of the ground truth to the preidction is based on the overlapping of their bounding boxes

- utils
  - ComputeBoxes
    - compute boxes from segmentation
  - EnlargeBBox
    - extends the margins of the boxes
    - example of use: prediction to give more context ([larger margins gives more contexte, allowing better recognition](https://link.springer.com/chapter/10.1007/978-3-030-86159-9_3))
  - RoIPooling
    - pool a tensor given various boxes
  - TextConverter
    - convert the text to indices and back
  - ValTestStep
    - loop used in validation and test to store and compute metrics

- logs
  - to save the logs of the training etc

- model_save
  - to save the models you just trained

## Dataset & Task description

### Dataset

The dataset is composed of 1 000 documents, 100 docs/ type, 10 types.

The different types are:

- Albanese ID card (alb_id)

- Azerbaidjan passport (aze_passport)

- Spain ID card (esp_id)

- Estonia ID card (est_id)

- Finland ID card (fin_id)

- Greek passport (grc_passport)

- Latvian passport (lva_passport)

- Internal russian passport (russ_internalpassport)

- Serbian passport (srb_passport)

- Slovakian ID card (svk_id)

The field types covered are:

- **authority**: alb_id, aze_passport, grc_passport, srb_passport

- **birth_date**: alb_id, aze_passport, esp_id, est_id, fin_id, grc_passport, lva_passport, rus_internalpassport, srb_passport, svk_id

- **birth_place**: alb_id, aze_passport, grc_passport, lva_passport, srb_passport

- **issued_date**: alb_id, aze_passport, esp_id, fin_id, grc_passport, lva_passport, srb_passport, svk_id

- **expiry_date**: alb_id, aze_passport, esp_id, est_id, fin_id, grc_passport, lva_passport, srb_passport, svk_id

- **gender**: alb_id, aze_passport, esp_id, est_id, fin_id, grc_passport, lva_passport, rus_internalpassport, srb_passport, svk_id

- **id_number**: alb_id, aze_passport, esp_id, est_id, lva_passport, srb_passport, svk_id

- **name**: alb_id, aze_passport, esp_id, est_id, fin_id, grc_passport, lva_passport, rus_internalpassport, srb_passport, svk_id

- **surname**: alb_id, aze_passport, esp_id, est_id, fin_id, grc_passport, lva_passport, rus_internalpassport, srb_passport, svk_id

- **nationality**: alb_id, aze_passport, esp_id, est_id, fin_id, grc_passport, lva_passport, srb_passport, svk_id

- **number**: alb_id, aze_passport, esp_id, est_id, fin_id, grc_passport, lva_passport, rus_internalpassport, srb_passport, svk_id

- **number2**: est_id

- **number3**: est_id

- **type**: aze_passport, grc_passport, lva_passport, srb_passport

- **code**: aze_passport, fin_id, grc_passport, lva_passport, srb_passport

- **name_eng**: aze_passport, grc_passport

- **surname_eng**: aze_passport, grc_passport

- **authority_eng**: aze_passport

- **expiry_date_2**: aze_passport, srb_passport

- **mrz_line0**: aze_passport, grc_passport, lva_passport, srb_passport

- **mrz_line1**: aze_passport, grc_passport, lva_passport, srb_passport

- **surname_second**: esp_id

- **name_code**: esp_id

- **birth_date_2**: fin_id

- **birth_date_22**: fin_id

- **birth_place_eng**: grc_passport

- **birth_country**: grc_passport

- **height**: grc_passport, lva_passport

- **authority_line0**: lva_passport

- **authority_line1**: lva_passport

- **birth_place_line0**: rus_internalpassport

- **birth_place_line1**: rus_internalpassport

- **birth_place_line2**: rus_internalpassport

- **patronymic**: rus_internalpassport

- **residence_line0**: srb_passport

- **residence_line1**: srb_passport

- **issue_place**: svk_id

It is possible to merge some fields together to obtain more meaningful categories. It is up to your usage to determine the granularity of such merging strategy. The reverse relation (document type to field types) is available in the dataset via the file *templates.json*.

The dataset is organized as follow:

- alphabet.json
  - Contains the alphabet of the different types and the mapping from the original alphabet to the used alphabet in the case of low representation.
- annotations.json
  - Contains the annotations of every documents under the following format:

```json
{
    <image_name>: {
        « doc_type »: ,
        « camer_type »: ,
        « capture_condition »: ,
        « fields »: [
            {
                « label »: <text label>,
                « map_label »: <text label based on mapped alphabet>
                « type »: <type of field>,
                « x1 »: ,
                « y1 »: ,
                « x2 »: ,
                « y2 »:
            },
            …
        ]
    }
}
```

- templates.json
  - Contains the modelisation of the structure of each document types under the following format:

```json
{
    <type name>: {
        « h »: height of the format,
        « w »: width of the format,
        « fields »: [
            {
                « type »: <type of field>,
                « x1 »: ,
                « y1 »: ,
                « x2 »: ,
                « y2 »:
            },
            …
        ]
    }
}
```

- train/validation/test_list.txt
  - Contains the names of the documents used for respectively train, validation and test where one line correspond to one document.
