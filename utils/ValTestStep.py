import torch
import matplotlib.pyplot as plt
from metrics import calc_iou, calc_bb_recall, calc_bb_precision, calc_cer, CER_IoU, DetectionMetrics

def fuse(big_dic, small_dic):
    for k in small_dic:
        if k in big_dic:
            big_dic[k] += small_dic[k]
        else:
            big_dic[k] = small_dic[k]
    return big_dic

class ValTestStep:
    def __init__(self, logger, str_converter) -> None:
        self.total_ious = []
        self.nb_over_box = []
        self.nb_gtbox = []
        self.nb_detbox = []
        self.nb_corr_detbox = []
        self.rp_cs = []

        self.dist_tp = []
        self.gt_len_tp = []
        self.dist_tn = []
        self.gt_len_tn = []
        self.dist_fp = []
        self.gt_len_fp = []

        self.losses = []
        self.log = logger
        self.str_converter = str_converter
        self.det_metrics = DetectionMetrics()

    def clear(self):
        self.total_ious = []
        self.nb_over_box = []
        self.nb_gtbox = []
        self.nb_detbox = []
        self.nb_corr_detbox = []
        self.rp_cs = []

        self.dist_tp = []
        self.gt_len_tp = []
        self.dist_tn = []
        self.gt_len_tn = []
        self.dist_fp = []
        self.gt_len_fp = []
        self.losses = []
       
    def step(self, x, m, obj, pred_bb, pred_tr, y, loss, curr_epoch, batch_idx, mode):
        # if pred_tr is none then just compute the detection part
        self.losses.append(loss)

        for i, el in enumerate(x):
            o = obj[i]

            _m = m[i]
            curr_m = _m[0]
            for j in range(1,len(_m)):
                curr_m = curr_m + _m[j]

            curr_annots = y[i]
            curr_annots = curr_annots[curr_annots[:,4]!=-1]

            if len(pred_bb[i]) != 0:
                curr_pred_bb = pred_bb[i]
                tot_iou, over_b, nb_gt_b, nb_det_b, nb_corr_det_b, rp_c = self.det_metrics(curr_pred_bb, curr_annots, 0.7)

                self.total_ious.append(tot_iou)
                self.nb_over_box.append(over_b)
                self.nb_gtbox.append(nb_gt_b)
                self.nb_detbox.append(nb_det_b)
                self.nb_corr_detbox.append(nb_corr_det_b)
                self.rp_cs.append(rp_c)

                if pred_tr is not None:
                    curr_pred_tr = pred_tr[i].permute(1,0,2)

                    gt_txt = [t for t in self.str_converter.decode2(curr_annots[:,5:])]
                    gt_index = self.str_converter.encode(gt_txt)[0]
                    tr_index = self.str_converter.decode_index(curr_pred_tr.to(device="cpu"))

                    (tp_distances, tp_gt_len), (tn_distances, tn_gt_len), (fp_distances, fp_gt_len) = CER_IoU(tr_index, gt_index, curr_pred_bb, curr_annots[:,:4], curr_annots[:,4], self.asso_gttxt_penalty, 0.1)

                    self.dist_tp.append(tp_distances)
                    self.gt_len_tp.append(tp_gt_len)
                    self.dist_tn.append(tn_distances)
                    self.gt_len_tn.append(tn_gt_len)
                    self.dist_fp.append(fp_distances)
                    self.gt_len_fp.append(fp_gt_len)


    def aggregate(self, mode, curr_epoch=None):
        mean_iou = sum(self.total_ious)/sum(self.nb_over_box) if len(self.nb_over_box) > 0 else 0
        mean_rec = sum(self.nb_corr_detbox)/sum(self.nb_gtbox) if len(self.nb_gtbox) > 0 else 0
        mean_pre = sum(self.nb_corr_detbox)/sum(self.nb_detbox) if len(self.nb_detbox) > 0 else 0

        tot_distances = sum([sum(self.dist_tp), sum(self.dist_tn), sum(self.dist_fp)])
        tot_len = (sum(self.gt_len_tp)+sum(self.gt_len_fp))
        mean_cer_pen = tot_distances/tot_len if tot_len >0 else 1
        mean_tn_err = sum(self.dist_tn)/tot_distances if tot_distances > 0 else 0
        mean_fp_err = sum(self.dist_fp)/tot_distances if tot_distances > 0 else 0
        mean_cer_tp = sum(self.dist_tp)/sum(self.gt_len_tp) if len(self.gt_len_tp) >0 else 1

        self.losses = [l for l in self.losses if l is not None]
        mean_loss = sum(self.losses)/len(self.losses) if len(self.losses) > 0 else 0

        self.log("mean_iou_"+mode, mean_iou)
        self.log("mean_recall_"+mode, mean_rec)
        self.log("mean_precision_"+mode, mean_pre)
        self.log("mean_tn_err_"+mode, mean_tn_err)
        self.log("mean_fp_err_"+mode, mean_fp_err)
        self.log("mean_cer_pen_"+mode, mean_cer_pen)
        self.log("mean_cer_tp_"+mode, mean_cer_tp)
        self.log("mean_loss_"+mode, mean_loss)

        self.clear()
        metrics = {
            "mean_iou": mean_iou,
            "mean_rec": mean_rec,
            "mean_pre": mean_pre,
            "mean_cer_pen": mean_cer_pen,
            "mean_tn_err": mean_tn_err,
            "mean_fp_err": mean_fp_err,
            "mean_cer_tp": mean_cer_tp
        }
        return metrics