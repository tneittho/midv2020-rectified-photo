import torch
import math
import pytorch_lightning as pl
import cv2
import numpy as np
from shapely import geometry


class ComputeBoxes:
    def __init__(self, score_threshold, overlap_threshold, max_boxes, sol=False):
        self.segToBB = SegToBB(score_threshold)
        self.overlapThreshold = overlap_threshold
        self.max_boxes = max_boxes
        self.nms = NMS()
        self.sol = sol


    def compute_boxes_seg(self, obj):
        bb = []
        for i in range(obj.shape[0]):
            boxes = self.segToBB(obj[i])
            boxes = self.nms(boxes, self.overlapThreshold)
            if len(boxes) > self.max_boxes:
                areas = (boxes[:,2]-boxes[:,0])*(boxes[:,3]-boxes[:,1])
                order = areas.argsort(descending=True)
                boxes = torch.stack([boxes[i] for i in order[:self.max_boxes]])

            bb.append(boxes)
        return bb
    
    def __call__(self, obj):
        return self.compute_boxes_seg(obj)

class SegToBB(pl.LightningModule):

    def __init__(self, threshold=0.7):
        super(SegToBB, self).__init__()
        self.threshold = threshold

    def forward(self, x):
        # is of shape [C, H, W]

        mask = (x > self.threshold).to(torch.float32)
        mask = mask.cpu().detach().numpy().astype(np.uint8)
        boxes = []
        for el in mask:
            boxes.extend(self.find_boxes(el, min_area = 50))
        # boxes are of shape [nb boxes, 4, 2]
        bbxs = []
        for bb in boxes:
            bb = torch.from_numpy(bb)
            bb = torch.as_tensor([
                torch.min(bb[:,0]), # on all 4 corners, take the min x
                torch.min(bb[:,1]), # on all 4 corners, take the min y
                torch.max(bb[:,0]),
                torch.max(bb[:,1])
            ])
            # check empty boxes
            if (bb[2] - bb[0]) < 5 or (bb[3] - bb[1]) < 5:
                continue
            bbxs.append(bb)
        if len(bbxs) != 0:
            return torch.stack(bbxs).to(device=x.device)
        else:
            return torch.as_tensor([], device=x.device)
 
    def find_boxes(self, boxes_mask, min_area=10, n_max_boxes=math.inf):
        def validate_box(box):
            polygon = geometry.Polygon([point for point in box])

            if polygon.area > min_area:
                box = np.maximum(box, 0)
                box = np.stack(
                    (np.minimum(box[:,0], boxes_mask.shape[1]),
                    np.minimum(box[:,1], boxes_mask.shape[0])
                    ), axis=1
                )
                return box, polygon.area
            return None
        assert len(boxes_mask.shape) == 2, "input mask must be 2D array, got {}".format(boxes_mask.shape)

        # return a list of contours: a contours is a list of list of point -> shape [nb contours, nb lines in contours, 1, 2] (x,y)
        contours, _ = cv2.findContours(boxes_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        if contours is None:
            print("No contour found")
            return None

        found_boxes = []

        # h, w = boxes_mask.shape[:2]
        for c in contours:
            x, y, w, h = cv2.boundingRect(c)
            box = np.array([[x,y], [x+w, y], [x+w, y+h], [x, y+h]], dtype=int)
            found_boxes.append(validate_box(box))
            # boxes = self.separate_boxes(c)
            # boxes = [validate_box(b) for b in boxes]
            # found_boxes.extend(boxes)

        filtered_boxes = [fb for fb in found_boxes if fb is not None]
        selected_boxes = [fb for i, fb in enumerate(filtered_boxes) if i < n_max_boxes]
        
        return [fb[0] for fb in selected_boxes]


class NMS:

    def nms(self, boxes, overlap_threshold):
        """
        boxes has shape [batch, nb_boxes, 4] or [nb_boxes, 4]
        """
        if len(boxes.shape) == 3:
            return [self._nms(b,overlap_threshold) for b in boxes]
        else:
            return self._nms(boxes,overlap_threshold)

    def _nms(self, boxes, threshold):
        if len(boxes) == 0 or boxes.shape[0] == 0:
            return torch.as_tensor([], device=boxes.device)
        b = boxes.to(dtype=torch.float32, device=boxes.device)

        x1 = b[:,0]
        y1 = b[:,1]
        x2 = b[:,2]
        y2 = b[:,3]

        areas = (x2-x1)*(y2-y1)
        
        order = areas.argsort(descending=True)
        keep = []
        # pbar = tqdm(desc="bounding box nms", total=b.shape[0], leave=False)
        while len(order) > 0:

            idx_highest = order[-1]
            keep.append(b[idx_highest])

            order = order[:-1] # remove last element
            # compute the IoU of every boxes w.r.t. the current highest
            ## rearrange the corner to match order indexing
            xx1 = torch.index_select(x1, dim=-1, index = order)
            xx2 = torch.index_select(x2, dim=-1, index = order)
            yy1 = torch.index_select(y1, dim=-1, index = order)
            yy2 = torch.index_select(y2, dim=-1, index = order)
            ## compute the intersections
            xx1 = torch.max(xx1, x1[idx_highest])
            xx2 = torch.min(xx2, x2[idx_highest])
            yy1 = torch.max(yy1, y1[idx_highest])
            yy2 = torch.min(yy2, y2[idx_highest])

            ## find height and width
            w = xx2-xx1
            h = yy2-yy1
            ## take max with 0.0 to avoid negative w and h
            w = torch.clamp(w, min=0.0) # is zero if no overlap 
            h = torch.clamp(h, min=0.0)
            ## find the intersection bb with selected bb
            inter_area = w * h
            ## reorder the areas
            rem_area = torch.index_select(areas,dim = 0, index = order)

            # find the union of every prediction T with the selected one
            union = (rem_area - inter_area) + areas[idx_highest]
            IoU = inter_area / union
            
            # keep takes the elements that don't overlap too much with the selected box
            mask = IoU < threshold
            order = order[mask]
            # try:
            #     pbar.update(sum(mask !=True).item()+1)
            # except AttributeError as e:
            #     pbar.update(sum(mask!=True)+1)
        # pbar.close()
        return torch.tensor(keep) if len(keep)==0 else torch.stack(tuple(keep), dim=0)

    def __call__(self, boxes, overlap_threshold):
        return self.nms(boxes, overlap_threshold)
