from typing import List, Tuple
import torch
from torch import Tensor

class TextConverter:
    """
    This class convert a String into the corresponding Tensor with the corresponding indices from the variable *alphabet*
    """
    def __init__(self, alphabet: List[chr]) -> None:
        self.alphabet = alphabet
        self.dict = {v_key:i+1 for i,v_key in enumerate(self.alphabet)}
        self.rev_dict = {i+1:v_key for i, v_key in enumerate(self.alphabet)}
    
    def encode(self, texts: List[str]) -> Tuple[Tensor, Tensor]:
        """
        Encode a list of String into the corresponding tensors

        :returns encoded and length normalized tensor; corresponding lengths tensors
        """
        lengths = []
        encod_txts = []
        for txt in texts:
            lengths.append(len(txt))
            r = []
            for char in txt:
                r.append(self.dict[char])
            encod_txts.append(r)
        max_len = max(lengths)
        for e in encod_txts:
            e.extend([0 for _ in range(max_len-len(e))])
        
        return torch.LongTensor(encod_txts), torch.LongTensor(lengths)
    
    def decode(self, tensors: Tensor, raw=False):
        """
        Decode a list of Tensors into the corresponding Strings
        :param tensors: the tensors to decode
        :param raw: whether to decode as is or to aggregate characters (raw: aaabcc c, not raw: abc c)
        :return list of list of char with shape [nb_txt, txt_size]
        """
        txts = []
        for ten in tensors:
            if not raw:
                t = [ten[0]]
                for c in ten[1:]:
                    if c != t[-1]:
                        t.append(c)
                # remove blank characters
                t = [c for c in t if c != 0]
            else:
                t = ten
            
            if len(t) != 0:
                if isinstance(t[0], Tensor):
                    txts.append([self.rev_dict[c.item()] for c in t])
                else:
                    txts.append([self.rev_dict[c] for c in t])
            else:
                txts.append(t)
        return txts
        