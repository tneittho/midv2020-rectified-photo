class EnlargeBBox:

    def __init__(self, margin):
        self.margin = margin
    
    def enlarge_boxes(self, boxes, img_shape):
        # boxes has shape [nb_boxes, 4]
        new_b = []
        for b in boxes:
            n_b = [
                max(0, b[0]-self.margin),
                max(0, b[1]-self.margin),
                min(img_shape[1], b[2]+self.margin),
                min(img_shape[0], b[3]+self.margin)
            ]
            
            new_b.append(torch.as_tensor(n_b))
        if len(new_b) == 0:
            GlobalLogger.log(f"new_b {new_b} boxes {boxes}", "Train_"+get_name())
            return torch.tensor([], device=boxes.device)
        return torch.stack(new_b)
