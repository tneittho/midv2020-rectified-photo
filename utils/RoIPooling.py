class RoIPooling:

    def __init__(self, size) -> None:
        self.normalizer = SizeNormalizer(size)

    def pool(self, features, boxes):
        """
        features has shape [C,H,W]
        boxes has shape [nb_boxes, 4]
        """
        outputs = [] # has shape [nb_boxes, C, H, W]
        removed = []
        for i in range(boxes.shape[0]): # iter on the different bboxes
            bbox = boxes[i]
            if bbox[0] > bbox[2]:
                tmp = bbox[0]
                bbox[0] = bbox[2]
                bbox[2] = tmp
            if bbox[1] > bbox[3]:
                tmp = bbox[1]
                bbox[1] = bbox[3]
                bbox[3] = tmp
            bbox = bbox.long()

            im_pool = features[:, bbox[1]:(bbox[3]+1), bbox[0]:(bbox[2]+1)]
            
            if im_pool.shape[1] < 1 or im_pool.shape[2] < 1:
                # remove when height or width is zero or 
                # when the coordinates in the bounding boxes are reversed: (x1 > x2) and (y1 > y2) which should never occur
                removed.append(i)
                continue
            outputs.append(im_pool)
        
        outputs = self.normalizer(outputs)
        if len(outputs) < 1:
            return None, None
        return outputs, removed

import torch
import torch.nn as nn
from torchvision.transforms import Pad

class SizeNormalizer:

    def __init__(self, target_height) -> None:
        self.target_height = target_height
    
    def normalize(self, images):
        """
        Reformat images to the target_height and padd the les wide images to the widest image
        images is a list of tensor images of shape [C,H,W]
        """
        max_width = 0
        new_images = []
        for img in images:
            height_ratio = self.target_height/img.shape[1]
            target_width = int(img.shape[2]*height_ratio)
            if max_width < target_width:
                max_width = target_width
            
            new_images.append(self.resize(img, target_width))
        
        padd_imgs = []
        for img in new_images:
            img_width = img.shape[2]
            padd_diff = max_width - img_width
            padding = Pad((padd_diff//2,0,padd_diff-padd_diff//2,0))
            padd_imgs.append(padding(img))
        
        padd_imgs = torch.stack(padd_imgs)
        return padd_imgs

    def resize(self, img, width):
        pool = nn.AdaptiveAvgPool2d((self.target_height, width))
        return pool(img)
    
    def __call__(self, images):
        return self.normalize(images)