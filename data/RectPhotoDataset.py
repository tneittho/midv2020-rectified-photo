import json
import cv2
import torch
import random
from utils.TextConverter import TextConverter

class RectPhotoDataset:

    def __init__(self, database_path: str, mode="train", fold=0) -> None:
        """
        param: database_path: the path to the database with the images and the necessary files
        param: mode: takes value in ["train", "val_seen", "val_unseen", "test_seen", "test_unseen"] and will compose its content based on this mode
        param: fold: takes value in [0-9] and correspond to the current unseen type for test, the unseen type for validation being fold+1
        """
        self.database_path = database_path
        self.mode = mode
        self.doc_class = [
            "alb_id", "aze_passport", "esp_id", "est_id", "fin_id", "grc_passport", "lva_passport", "rus_internalpassport", "srb_passport", "svk_id"
        ]
        self.test_unseen = fold%len(self.doc_class)
        self.val_unseen = fold+1%len(self.doc_class)

        with open(database_path+"/alphabet.json", "r", encoding='utf-8') as a:
            alphabet = json.load(a)
        self.textConverter = TextConverter(alphabet["alphabet"])

        with open(f"{self.database_path}/models.json", "r", encoding="utf-8") as m:
            self.templates = json.load(m)

    def compose(self):
        """
        Compose the dataset by loading the files corresponding to the mode and their annotations
        """
        with open(f"{self.database_path}/annotations.json", "r", encoding="utf-8") as a:
        #with open(f"{self.database_path}/annots.json", "r", encoding="utf-8") as a:
            annotations = json.load(a)
        
        self.doc_names = self._compose_fold()

        self.annots = {k:v for k,v in annotations.items() if k in self.doc_names}
        self.samples = self.doc_names
        random.shuffle(self.samples)
        
                
    def _compose_fold(self):
        """
        returns: The list of document name for the current mode
        """
        indic = {
            "train": self._train,
            "val_seen": self._val_seen,
            "val_unseen": self._val_unseen,
            "test_seen": self._test_seen,
            "test_unseen": self._test_unseen
        }
        return indic[self.mode]()

    def _convert_annotations(self, annots:dict):
        """
        Convert the annotations of a document to Tensor format.
        :param annots has shape {
            "doc_type": <doc_type>,
            "camera_type": <camera_type>,
            "capture_condition": <0-8>,
            "fields": [
                {
                    "x1": 717,
                    "y1": 977,
                    "x2": 799,
                    "y2": 1020,
                    "label": "MB",
                    "map_label": "MB",
                    "type": "authority"
                },
                ...
            ]
        }
        :returns the Tensor containing the list of fields with their positions and the associated text, the Tensor of the lengths of texts.
        """
        all_texts = [el["map_label"] for el in annots["fields"]]

        encoded_txts, all_lengths = self.textConverter.encode(all_texts)

        res = torch.stack([
            torch.cat([
                torch.as_tensor([
                    annots["fields"][i]["x1"],
                    annots["fields"][i]["y1"],
                    annots["fields"][i]["x2"],
                    annots["fields"][i]["y2"],
                ]),
                encoded_txts[i]
            ])
            for i in range(len(annots["fields"]))
        ])

        return res, all_lengths

    def _convert_template(self, template, image_shape):
        """
        Convert the template to a binarized image
        """
        mask = torch.zeros((1,*image_shape[1:]))

        for f in template["fields"]:
            mask[:,f["y1"]:f["y2"], f["x1"]:f["x2"]] = 1
        return mask
    
    def __len__(self):
        return len(self.doc_names)

    def __getitem__(self, idx):
        """
        Get an image and the associated template and annotations
        """
        assert idx <= len(self)
        doc_name = self.samples[idx]
        doc_annot = self.annots[doc_name]
        template = self.templates[doc_annot["doc_type"]]

        img_path = f"{self.database_path}/images/{doc_name}.jpg"
        img = cv2.imread(img_path, cv2.IMREAD_GRAYSCALE) # can be also GRAYSCALE

        doc_annot, lengths = self._convert_annotations(doc_annot)

        img = img.reshape(1, *img.shape)

        img = img/255
        img = torch.from_numpy(img).to(dtype=torch.float32)

        template = self._convert_template(template, img.shape)

        return img, (doc_annot, lengths), template


    def _train(self):
        with open(f"{self.database_path}/train_list.txt", "r") as f:
            # authorize comments in the document names files
            doc_list = [l.split("#")[0] for l in f.readlines() if l[0]!="#"]
        out_types = [self.doc_class[self.test_unseen], self.doc_class[self.val_unseen]]
        # remove documents that belongs to the unseen types
        doc_list = [doc_name.strip() for doc_name in doc_list if out_types[0] not in doc_name or out_types[1] not in doc_name]
        return doc_list
    
    def _val_seen(self):
        with open(f"{self.database_path}/validation_list.txt", "r") as f:
            doc_list = [l.split("#")[0] for l in f.readlines() if l[0]!="#"]
        out_types = [self.doc_class[self.test_unseen], self.doc_class[self.val_unseen]]
        doc_list = [doc_name.strip() for doc_name in doc_list if out_types[0] not in doc_name or out_types[1] not in doc_name]
        return doc_list
    
    def _val_unseen(self):
        with open(f"{self.database_path}/validation_list.txt", "r") as f:
            doc_list = [l.split("#")[0] for l in f.readlines() if l[0]!="#"]
        doc_list = [doc_name.strip() for doc_name in doc_list if self.doc_class[self.val_unseen] in doc_name]
        return doc_list

    def _test_seen(self):
        with open(f"{self.database_path}/test_list.txt", "r") as f:
            doc_list = [l.split("#")[0] for l in f.readlines() if l[0]!="#"]
        out_types = [self.doc_class[self.test_unseen], self.doc_class[self.val_unseen]]
        doc_list = [doc_name.strip() for doc_name in doc_list if out_types[0] not in doc_name or out_types[1] not in doc_name]
        return doc_list
    
    def _test_unseen(self):
        with open(f"{self.database_path}/test_list.txt", "r") as f:
            doc_list = [l.split("#")[0] for l in f.readlines() if l[0]!="#"]
        doc_list = [doc_name.strip() for doc_name in doc_list if self.doc_class[self.test_unseen] in doc_name]
        return doc_list

