from torch.utils.data.dataloader import DataLoader
from data.Collate import Collate

import pytorch_lightning as pl

class RectPhotoDataloader(pl.LightningDataModule):

    def __init__(self, train_dataset, val_dataset, test_dataset, batch_size=2, max_height=500):
        super(RectPhotoDataloader, self).__init__()
        self.train_dataset = train_dataset
        self.val_dataset = val_dataset
        self.test_dataset = test_dataset
        self.collate = Collate(max_height)
        self.batch_size = batch_size

    def  prepare_data(self):
        return super().prepare_data()
    
    def prepare_data_per_node(self):
        return super().prepare_data_per_node()
    
    def setup(self, stage = None) -> None:
        return super().setup(stage)
    
    def train_dataloader(self):
        return DataLoader(self.train_dataset, batch_size=self.batch_size, collate_fn=self.collate)
    
    def val_dataloader(self):
        return [DataLoader(self.val_dataset[i], batch_size=self.batch_size, collate_fn=self.collate) for i in range(len(self.val_dataset))]
    
    def test_dataloader(self):
        return [DataLoader(self.test_dataset[i], batch_size=self.batch_size, collate_fn=self.collate) for i in range(len(self.test_dataset))]
    
    def __post_init__(cls):
        super().__init__()