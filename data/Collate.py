import torch
from torchvision.transforms import Pad

class Collate:

    def __init__(self, max_height):
        self.max_height = max_height

    def __call__(self, batch):
        batch = [el for el in batch if el is not None]
        images, annots, modelisations = zip(*batch)
        annots, lengths = zip(*annots)

        # annots are tensors of shape [B, nb_fields, 5+max_txt_len]

        # check what is the max width for the max heights
        # for each annots and each modelisation, readapt them to the size of their respective images
        # resize pad each images
        # return the images, annots and modelisations
        max_width = 0
        new_annots = []
        new_modelisations = []
        new_images = []
        for img, annot, modelisation in zip(images, annots, modelisations):
            height_ratio = self.max_height/img.shape[1]
            target_width = int(img.shape[2]*height_ratio)
            if max_width < target_width:
                max_width = target_width

            new_annots.append(self.resize_annots(img, annot, target_width))
            new_modelisations.append(self.resize_image(modelisation, target_width))
            new_images.append(self.resize_image(img, target_width))
        
        padd_imgs = []
        for img in new_images:
            img_width = img.shape[2]
            padd_diff = max_width - img_width
            padding = Pad((0,0,padd_diff,0))
            padd_imgs.append(padding(img))
        
        padd_imgs = torch.stack(padd_imgs)

        padd_model = []
        for mod in new_modelisations:
            mod_width = mod.shape[2]
            padd_diff = max_width-mod_width
            padding = Pad((0,0,padd_diff,0))
            padd_model.append(padding(mod))
        
        padd_model = torch.stack(padd_model)

        # padd_imgs [batch, C, H, W]
        # new_annots [batch, nb_fields, 5+txt_length]
        # lengths [batch, nb_fields]
        # new_modelisations [batch, C, H, W]
        return padd_imgs, (new_annots, lengths), padd_model
        
        

    def resize_annots(self, image, annot, target_width):
        return self.rescale_bb(image.shape[1:], (self.max_height, target_width), annot)
    
    def resize_modelisation(self, modelisation, target_width):
        # Convert the modelisation to a mask in the size of the input image
        # Pool the mask in the size for the target_width
        pool = torch.nn.AdaptiveAvgPool2d((self.max_height, target_width))
        return pool(modelisation)
    
    def resize_image(self, image, target_width):
        pool = torch.nn.AdaptiveAvgPool2d((self.max_height, target_width))
        return pool(image)
    
    def rescale_bb(self, size_origin, size_target, bbxs):
        """
        bbxs has shape [nb_bboxes, 4+(x1,y1,x2,y2)]
        """
        try:
            relative_x1 = bbxs[:,0]/size_origin[1]
        except TypeError as e:
            print("bbxs", bbxs.shape)
            print("size_origin", size_origin)
            print("size target", size_target)
        relative_y1 = bbxs[:,1]/size_origin[0]
        relative_x2 = bbxs[:,2]/size_origin[1]
        relative_y2 = bbxs[:,3]/size_origin[0]

        target_x1 = relative_x1*size_target[1]
        target_y1 = relative_y1*size_target[0]
        target_x2 = relative_x2*size_target[1]
        target_y2 = relative_y2*size_target[0]

        bb =  torch.stack([target_x1,target_y1,target_x2,target_y2], dim=1).int()
        return torch.cat([bb,bbxs[:,4:]], dim=1)
