# Text Localisation and Recognition for Unseen ID Types

The task addresses text field localisation and recognition in ID documents in the context of ID types that have not been seen at training time. We give as input the rectified document images and the associated template. The goal of the task is the output of the text localisation, the name of the field and the textual content of the document.

## TLR-UIDT on *"rectified photos"*

From the *"rectified photos"* dataset, we design three sets for training, validation and test. As we aim to evaluate the performances of the systems on unseen ID types, we keep a type only for validation and another one only for test (Table 1.). We also add examples in validation and test of the same types as in the training in order to evaluate the robustness of the system and prevent overfitting. In order to perform an overall evaluation on the $10$ types, we propose a 10-fold cross-validation following the settings shown in Table1. We provide with the annotations the **train, validation, test** splits of each document types.

| "rectified photos" | Type of ID                        | NB of images | Image distribution                                  |
|--------------------|-----------------------------------|--------------|-----------------------------------------------------|
| Train              | 8 types                           | 640          | 80 imgs/type                                        |
| Validation         | 8 seen types +1 unseen type       | 180          | 10 imgs/seen type +100 images from unseen val type  |
| Train              | 8 seen types +1 other unseen type | 180          | 10 imgs/seen type +100 images from unseen test type |

We separate the different documents into **train**, **validation** and **test** so that the evaluation is always performed on the same documents. We also chose the documents so that it also reflect the capture condition distribution found in the dataset. The list of documents for each set can be found in the files *train_list.txt*, *val_list.txt*, *test_list.txt*.

We propose a pytorch dataset and dataloader implementation that can be used to connect to your training pipeline.
