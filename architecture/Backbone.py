import torch
import torch.nn as nn
import pytorch_lightning as pl

from architecture.CBR import CBR


class Backbone(pl.LightningModule):

    def __init__(self, in_chan, channels=[64,128,256], layers=[1,1,1], pools=[True,True,True]) -> None:
        super(Backbone, self).__init__()
        self.base = CBR(in_chan, channels[0], kernel=7, stride=1, padding=3)
        self.max_pool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)

        self.base_channels = channels[0]

        self.layers = nn.ModuleList(self._make_layers(layers, channels, pools))
        self.out_conv = CBR(channels[-1], channels[-1], 3, 1, 1)

        self.out_chan = [channels[0]]
        self.out_chan.extend([self.layers[i][-2].out_channels if pool else self.layers[i][-1].out_channels for i, pool in enumerate(pools)])

    def _make_layers(self, layers, channels, pools):
        res = []
        for i in range(len(layers)):
            out_channels = channels[i]
            nb_layers = layers[i]
            pooling = pools[i]

            l = [CBR(self.base_channels, out_channels, stride=1)]
            self.base_channels = out_channels
            for _ in range(1, nb_layers):
                l.append(CBR(self.base_channels, out_channels))

            if pooling:
                l.append(nn.MaxPool2d(kernel_size=3, stride=2, padding=1))
            res.append(nn.Sequential(*l))
        return res

    def forward(self, x):
        x = self.base(x)
        x = self.max_pool(x)
        out = [x]
        for layer in self.layers:
            x = layer(x)
            out.append(x)
        out[-1] = self.out_conv(out[-1])
        return out
