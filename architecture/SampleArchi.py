import torch
import torch.nn as nn
import pytorch_lightning as pl
import torch.nn.functional as F

from architecture.Loss import BCELoss, TranscriptionLoss
from utils import ComputeBoxes, RoIPooling, EnlargeBBox, ValTestStep
from architecture.Backbone import Backbone
from architecture.Segmentation import Segmentation
from architecture.RecognitionHead import RecognitionHead
from architecture.Up import Up

class SampleArchi(pl.LightningModule):

    def __init__(self, kwargs) -> None:
        super(SampleArchi, self).__init__()
        

        self._init_params(kwargs)
        self._init_network()
        self._init_losses()
        self.vt_step_unseen = ValTestStep(self.log, self.str_converter)
        self.vt_step_seen = ValTestStep(self.log, self.str_converter)
    
    def _init_params(self, kwargs):
        self.lr = 0.001
        self.max_boxes = 20
        self.score_threshold = 0.85
        self.overlap_threshold = 0.8
        self.pool_height = 20
        self.pretrain_epoch = 10
        self.loss_weights = [1.,1.]
        self.str_converter = kwargs["str_converter"]
        self.pretrain = kwargs["pretrain"]
        self.image_chan = kwargs["image_chan"]
        self.max_boxes = kwargs["max_boxes"]
        self.score_threshold = kwargs["score_threshold"]
        self.overlap_threshold = kwargs["overlap_threshold"]
        self.alphabet_size = len(self.str_converter.alphabet)
    
    def _init_network(self):
        self.backbone = Backbone(self.image_chan)
        self.up = Up([64, 64, 128, 256]) # the up part of the Unet
        self.segmentation = Segmentation(64, 64, 1)
        self.recognition = RecognitionHead(1, self.alphabet_size+1, 256)
        self.computeBB = ComputeBoxes(self.score_threshold, self.overlap_threshold, self.max_boxes)
        self.pool = RoIPooling(self.pool_height)
        self.enlarge = EnlargeBBox(15)

    def _init_losses(self):
        self.tr_loss = TranscriptionLoss()
        self.seg_loss = BCELoss()
    
    def configure_optimizers(self):
        return torch.optim.Adam(self.parameters(), lr=self.lr)
    
    def forward(self, x):
        xs = self.backbone(x)
        features = self.up(xs)
        seg = self.segmentation(features[-1], x.shape[2:])

        if self.pretrain:
            if self.current_epoch >= self.pretrain_epoch:
                self.pretrain = False
            return seg

        bb = self.computeBB(seg)

        transcripts = []
        for i in range(len(bb)):
            if len(bb[i]) ==0:
                transcripts.append(torch.as_tensor([], device=x.device))
                continue
            bb = self.enlarge.enlarge_boxes(bb[i], x.shape[2:])
            pooled_f, _ = self.pool.pool(x[i], bb)
            trans = self.recognition(pooled_f)
            transcripts.append(trans)
        
        return seg, bb, transcripts

    def training_step(self, batch):
        x,y,m = batch
        y,lengths = y
        if self.pretrain:
            seg = self(x)
            seg_loss = self.seg_loss(seg, y, x.shape[2:])
            self.log("seg_loss", seg_loss)
            self.log("loss", seg_loss)
            return seg_loss
        else:
            seg, bb, tr = self(x)

            seg_loss = self.objLoss(seg, y, x.shape[2:])
            tr_loss = self.tr_loss(tr, y, lengths, bb)
            self.log("seg_loss", seg_loss)
            self.log("tr_loss", tr_loss)
            self.log("loss", seg_loss+tr_loss)
            return self.loss_weight[0]*seg_loss + self.loss_weight[1]*tr_loss

    def validation_step(self, batch, batch_idx, dataloader_idx):
        x,y,m = batch
        y,lengths = y
        if self.pretrain:
            seg = self(x)
            bb = self.computeBB(seg)
            tr = None
            loss = self.seg_loss(seg, y, x.shape[2:])
        else:
            seg, bb, tr = self(x)
            seg_loss = self.seg_loss(seg, y, x.shape[2:])
            tr_loss = self.tr_loss(tr, y, lengths, bb)
            loss = sum([loss*weight for loss, weight in zip([seg_loss, tr_loss], self.lambdas)])
        
        if dataloader_idx == 0:
            self.vt_step_unseen.step(x,m,seg,bb,tr,y,loss,self.current_epoch,batch_idx,mode="val_unseen")
        else:
            self.vt_step_seen.step(x,m,seg,bb,tr,y,loss,self.current_epoch,batch_idx,mode="val_seen")
    
    def test_step(self, batch, batch_idx, dataloader_idx):
        x,y,m = batch
        y,lengths = y
        if self.pretrain:
            seg = self(x)
            bb = self.computeBB(seg)
            tr = None
            loss = self.seg_loss(seg, y, x.shape[2:])
        else:
            seg, bb, tr = self(x)
            seg_loss = self.seg_loss(seg, y, x.shape[2:])
            tr_loss = self.tr_loss(tr, y, lengths, bb)
            loss = sum([loss*weight for loss, weight in zip([seg_loss, tr_loss], self.lambdas)])
        
        if dataloader_idx == 0:
            self.vt_step_unseen.step(x,m,seg,bb,tr,y,loss,self.current_epoch,batch_idx,mode="test_unseen")
        else:
            self.vt_step_seen.step(x,m,seg,bb,tr,y,loss,self.current_epoch,batch_idx,mode="test_seen") 
    
    def on_validation_epoch_end(self):
        metrics_us = self.vt_step_unseen.aggregate(mode="val_unseen", curr_epoch=self.current_epoch)
        metrics_s = self.vt_step_seen.aggregate(mode="val_seen", curr_epoch=self.current_epoch)
        
        alpha = 0.5
        beta = 0.5
        
        self.log("average_cer_mix_val", alpha*metrics_us["mean_cer_pen"]+beta*metrics_s["mean_cer_pen"])
        # self.validation_step_outputs.clear()
    
    def on_test_epoch_end(self):
        self.vt_step_unseen.aggregate(mode="test_unseen")
        self.vt_step_seen.aggregate(mode="test_seen")
        # self.test_step_outputs.clear()






