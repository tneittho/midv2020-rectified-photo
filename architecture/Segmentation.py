import torch
import torch.nn as nn
import pytorch_lightning as pl
from architecture.CBR import CBR
import torch.nn.functional as F

class Segmentation(pl.LightningModule):

    def __init__(self, in_channels, feature_size, out_chan):
        super(Segmentation, self).__init__()
        self.layers = nn.ModuleList([
            CBR(in_channels, feature_size, 3, 1, 1),
            CBR(feature_size, feature_size, 3, 1, 1),
            CBR(feature_size, out_chan, 3, 1, 1)
        ])
        self.up = nn.Upsample(scale_factor=2, mode="bilinear")
    
    def forward(self, x, image_size):
        out = self.up(x)

        diffY = image_size[0]-out.shape[2]
        diffX = image_size[1]-out.shape[3]
        out = F.pad(out, [diffX // 2, diffX - diffX // 2,
                        diffY // 2, diffY - diffY // 2
        ])

        for i in range(len(self.layers)):
            out = self.layers[i](out)
        
        return out