import torch
import torch.nn as nn
import pytorch_lightning as pl

class BLSTM(pl.LightningModule):
    def __init__(self, in_dim, hidden_dim, out_dim, dtype=torch.float32):
        super(BLSTM, self).__init__()

        self.rnn = nn.LSTM(in_dim, hidden_dim, bidirectional=True, dtype=dtype)
        self.embedding = nn.Linear(hidden_dim*2, out_dim, dtype=dtype)
    
    def forward(self, x):
        recurrent, _ = self.rnn(x)
        t, b, h = recurrent.size()
        t_rec = recurrent.view(t*b, h)
        out = self.embedding(t_rec)
        out = out.view(t, b, -1)
        return out