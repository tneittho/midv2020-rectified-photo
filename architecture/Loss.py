import torch
from torch import nn
import pytorch_lightning as pl

class BCELoss(pl.LightningModule):

    def __init__(self, seg_entities=False):
        super(BCELoss, self).__init__()
        self.seg_entities=seg_entities

    def forward(self, y_hat, y, image_size):
        """
        y_hat has shape [B,C,H,W]
        y has shape [B, nb_fields, 4+ (x1,y1,x2,y2,+txt_len)]
        """
        losses = []
        for i in range(y_hat.shape[0]):
            obj = y_hat[i]
            bbox_annot = y[i]
            bbox_annot = bbox_annot[bbox_annot[:,4]!=-1]

            obj = torch.clamp(obj, 1e-4, 1-1e-4)
            target = self.build_target(bbox_annot.int(), obj.shape)

            # compute the balanced cross entropy
            # Loss = -B y* log(ŷ) - (1-B)(1-y*) log(1-ŷ)
            # B = 1 - (Sum y*)/|Y*|
            B = 1 - torch.sum(target) / torch.numel(target)
            loss = -B*torch.sum(target*torch.log(obj)) - (1-B)*torch.sum((1-target)*torch.log(1-obj))

            losses.append(loss.sum())
        return torch.stack(losses).mean(dim=0, keepdim=True) 

    def build_target(self, bbox_annot, feature_size):
        target_map = torch.zeros(feature_size, device=bbox_annot.device)
        for bb in bbox_annot:
            target_map[bb[4] if self.seg_entities else 0, bb[1]:bb[3], bb[0]:bb[2]] = 1
        return target_map
        

class TranscriptionLoss(pl.LightningModule):

    def __init__(self):
        super().__init__()
        self.criterion = nn.CTCLoss(zero_infinity=True, reduction='sum')

    def forward(self, y_hat, y, lengths, boxes):
        """
        y_hat   [batch, nb_det_fields, nb_entities]
        y       [batch, nb_gt_fields, 5+txt_len (x1,y1,x2,y2,class,txt)]
        lengths [batch, nb_gt_fields]
        boxes   [batch, nb_det_fields, (x1,y1,x2,y2)]
        """
        tr_losses = []
        for i in range(len(y_hat)):
            tr = y_hat[i]
            eps_tr = torch.clamp(tr, 1e-4, 1-1e-4)
            if len(eps_tr.shape) == 3:
                eps_tr = eps_tr.permute(1,0,2)
            else:
                print("eps dim pb",eps_tr.shape, "corr box", boxes[i])
                continue
            annots = y[i]
            annots = annots[annots[:,4]!=-1]

            length = lengths[i]
            box = boxes[i]
            try:
                pred_size = torch.as_tensor([tr.shape[0]]*tr.shape[1], device=tr.device, dtype=torch.int64)
            except IndexError as e:
                return None
            IoU = calc_iou(box, annots[:,:4])
            DIoU = IoU
            DIoU_max, DIoU_argmax = torch.max(DIoU, dim=1)

            corr_detect = DIoU_max > 0.5
            bbdt_bbgt = []
            bbdt_solo = []

            for i in range(len(corr_detect)):
                if corr_detect[i]: # if the box overlap with a bbgt
                    bbdt_bbgt.append([eps_tr[i], annots[DIoU_argmax[i], 5:], length[DIoU_argmax[i]], pred_size[i]])
                else: # no bbdt overlap with bbgt
                    bbdt_solo.append([eps_tr[i], torch.as_tensor([0 for _ in range(len(annots[0,5:]))], device=eps_tr.device), torch.as_tensor(0, device=eps_tr.device), pred_size[i]])
            
            associations = bbdt_bbgt + bbdt_solo
            assigned_annots = annots[DIoU_argmax, 5:]
            assigned_lengths = length[DIoU_argmax]

            assigned_annots = torch.stack([a[1] for a in associations])
            assigned_trans = torch.stack([a[0] for a in associations])
            assigned_lengths = torch.stack([a[2] for a in associations])
            assigned_pred_len = torch.stack([a[3] for a in associations])
            
            loss = self.criterion(assigned_trans.permute(1,0,2).log(), assigned_annots, assigned_pred_len, assigned_lengths)
            tr_losses.append(loss)
            
        if len(tr_losses) == 0:
            print("y_hat", len(y_hat), [h.shape for h in y_hat])
            print("tr losses", tr_losses)
            return torch.zeros((1), device=y_hat[0].device)
        else:
            return torch.stack(tr_losses).mean(dim=0, keepdim=True)
