import pytorch_lightning as pl
import torch.nn as nn
import torch
from architecture.CBR import CBR
from architecture.BLSTM import BLSTM

class RecognitionHead(pl.LightningModule):
    def __init__(self, in_chan, alphabet_len, feature_size, dtype=torch.float32) -> None:
        super(RecognitionHead, self).__init__()
        self.alphabet_len = alphabet_len
        self.layers = nn.ModuleList([
            CBR(in_chan, feature_size, 3,1,1, dtype=dtype),
            CBR(feature_size, feature_size, 3,1,1, dtype=dtype),
            CBR(feature_size, feature_size, 3,1,1,dtype=dtype)
        ])
        self.rnn = nn.ModuleList([
            BLSTM(feature_size, feature_size, feature_size, dtype=dtype),
            BLSTM(feature_size, feature_size, feature_size, dtype=dtype)
        ])

        self.out = nn.ModuleList([
            nn.Linear(feature_size, self.alphabet_len+1, dtype=dtype),
            nn.Softmax(dim=2)
        ])
    
    def forward(self, x):
        """
        Output a tensor of shape [B, H, W, alphabet+1]
        """
        out = x
        for l in self.layers:
            out = l(out)
        # out has shape [B,C,H,W]
        out = torch.sum(out, dim=2)
        out = out.permute(2,0,1)
        for r in self.rnn:
            out = r(out)
        for o in self.out:
            out = o(out)
        return out
