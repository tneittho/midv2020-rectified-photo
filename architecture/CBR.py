import torch.nn as nn
import torch
import pytorch_lightning as pl

class CBR(pl.LightningModule):

    def __init__(self, in_chan, out_chan, kernel=3, stride=1, padding=1, dtype=torch.float32) -> None:
        super(CBR, self).__init__()
        self.conv = nn.Conv2d(in_chan, out_chan, kernel, stride, padding, dtype=dtype)#, device="cuda")
        self.relu = nn.ReLU()
        self.bn = nn.BatchNorm2d(out_chan, dtype=dtype)#, device="cuda")
        self.out_channels = out_chan
    
    def forward(self, x):
        return self.bn(self.relu(self.conv(x)))
