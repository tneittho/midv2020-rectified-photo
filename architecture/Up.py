import torch.nn as nn
import torch
import pytorch_lightning as pl
from architecture.CBR import CBR

class Up(pl.LightningModule):

    def __init__(self, channels, dtype=torch.float32):
        super(Up, self).__init__()
        u_b = [
            UBlock(channels[i], channels[i+1], dtype=dtype)
            for i in range(len(channels)-1)
        ]
        u_b.reverse()
        self.u_blocks = nn.ModuleList(u_b)
        self.out_chan = self.u_blocks[-1].out_conv.out_channels
    
    def forward(self, x):
        x.reverse()
        out = [x[0]]
        for i in range(len(x)-1):
            out.append(self.u_blocks[i](x[i+1], out[-1]))
        return out


class UBlock(pl.LightningModule):

    def __init__(self, channel_a, channel_b, dtype=torch.float32):
        super(UBlock, self).__init__()
        self.up_conv = CBR(channel_b, channel_a, 3, 1, 1, dtype=dtype)
        self.cat_conv = CBR(channel_a*2, channel_a, 3, 1, 1, dtype=dtype)
        self.out_conv = CBR(channel_a, channel_a, 3, 1, 1, dtype=dtype)
    
    def forward(self, x_a, x_b):
        to_up = nn.AdaptiveAvgPool2d(x_a.shape[2:])
        br_b = to_up(x_b)
        br_b = self.up_conv(br_b)
        
        out = torch.cat([x_a, br_b], dim=1)
        out = self.cat_conv(out)
        out = self.out_conv(out)
        return out